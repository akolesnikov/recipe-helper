from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain, CMakeDeps
from conan.tools.build import check_min_cppstd
from conan.tools.scm import Version
import os


class BaseConanFile(object):
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "fPIC": [True, False]
    }
    default_options = {
        "shared": True,
        "fPIC": True
    }

    _cmake = None
    _cmake_toolchain = None

    @property
    def cmake(self):
        if not self._cmake:
            self._cmake = CMake(self)
            self._cmake.configure()

        return self._cmake

    @property
    def cmake_toolchain(self):
        if not self._cmake_toolchain:
            self._cmake_toolchain = CMakeToolchain(self)

        return self._cmake_toolchain

    @property
    def qt_options(self):
        return self.options["qt"]

    @property
    def user_name(self):
        return self.user.split('+')[0]

    def requirements(self):
        self.requires(f"extra-cmake-modules/{self.version}@{self.user_name}+extra-cmake-modules/{self.channel}")
        self.requires("qt/5.15.7")

        if self.conan_data:
            if "requirements" in self.conan_data:
                os_name = str(self.settings.os).lower()
                if os_name in self.conan_data["requirements"]:
                    for req in self.conan_data["requirements"][os_name]:
                        self.requires(req)

                if "all" in self.conan_data["requirements"]:
                    for req in self.conan_data["requirements"]["all"]:
                        self.requires(req)

    def build_requirements(self):
        if self.conan_data:
            if "tool_requirements" in self.conan_data:
                os_name = str(self.settings.os).lower()
                if os_name in self.conan_data["tool_requirements"]:
                    for req in self.conan_data["tool_requirements"][os_name]:
                        self.tool_requires(req)

                if "all" in self.conan_data["tool_requirements"]:
                    for req in self.conan_data["tool_requirements"]["all"]:
                        self.tool_requires(req)

    def validate(self):
        check_min_cppstd(self, "17")

    def generate(self):
        qt = self.dependencies["qt"]
        ecm = self.dependencies["extra-cmake-modules"]
        self.cmake_toolchain.variables["REQUIRED_QT_VERSION"] = qt.ref.version
        self.cmake_toolchain.variables["QT_MAJOR_VERSION"] = Version(qt.ref.version).major
        self.cmake_toolchain.variables["REQUIRED_ECM_VERSION"] = ecm.ref.version
        self.cmake_toolchain.generate()

        deps = CMakeDeps(self)
        deps.generate()

    def layout(self):
        self.folders.source = "."
        build_type = str(self.settings.build_type).lower()
        self.folders.build = f"cmake-build-{build_type}"
        self.folders.generators = os.path.join(self.folders.build, "conan")

    def build(self):
        self.cmake.build()
        #cmake.test()

    def package(self):
        self.cmake.install()


class RecipeHelper(ConanFile):
    name = "recipe-helper"
    version = "0.1"
